#version 430 core

in layout(location = 0) vec3 position;
in layout(location = 1) vec2 vertexUV;
in layout(location = 5) vec3 normalCoordinates;
uniform layout(location = 2) mat4 vpTransformation;
uniform layout(location = 4) mat4 modelTransformation;

out vec2 UV;
out vec3 Position_worldspace;
out vec3 Normal_cameraspace;
out vec3 EyeDirection_cameraspace;
out vec3 LightDirection_cameraspace;
out vec3 Normal2_cameraspace;
out vec3 LightDirection2_cameraspace;
uniform layout(location = 6) vec3 lightCoordinates;
uniform layout(location = 9) vec3 light2Coordinates;

void main()
{
	gl_Position = vpTransformation * modelTransformation * vec4(position, 1.0);
	UV = vertexUV;

	Position_worldspace = (modelTransformation * vec4(position,1)).xyz;
	vec3 vertexPosition_cameraspace = ( modelTransformation * vpTransformation * vec4(position,1)).xyz;
	EyeDirection_cameraspace = vec3(0,0,0) - vertexPosition_cameraspace;

	vec3 LightPosition_cameraspace = ( vpTransformation * vec4(lightCoordinates,1)).xyz;
	LightDirection_cameraspace = LightPosition_cameraspace + EyeDirection_cameraspace;
	Normal_cameraspace = ( modelTransformation * vpTransformation * vec4(normalCoordinates,0)).xyz; // Only correct if ModelMatrix does not scale the model ! Use its inverse transpose if not.

	vec3 LightPosition2_cameraspace = ( vpTransformation * vec4(lightCoordinates,1)).xyz;
	LightDirection2_cameraspace = LightPosition_cameraspace + EyeDirection_cameraspace;
	Normal2_cameraspace = ( modelTransformation * vpTransformation * vec4(normalCoordinates,0)).xyz; // Only correct if ModelMatrix does not scale the model ! Use its inverse transpose if not.
}