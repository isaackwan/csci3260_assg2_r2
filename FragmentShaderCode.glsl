#version 430 core

in vec2 UV;
uniform layout(location = 3) sampler2D myTextureSampler;
out vec3 color;

in vec3 Position_worldspace;
in vec3 Normal_cameraspace;
in vec3 Normal2_cameraspace;
in vec3 EyeDirection_cameraspace;
in vec3 LightDirection_cameraspace;
in vec3 LightDirection2_cameraspace;
uniform layout(location = 6) vec3 lightCoordinates;
uniform layout(location = 7) float diffuseLightPower;
uniform layout(location = 8) float specularLightPower;
uniform layout(location = 9) vec3 light2Coordinates;

// this code takes reference from http://www.opengl-tutorial.org/beginners-tutorials/tutorial-8-basic-shading/ & TA's code

void main()
{
	vec3 LightColor = vec3(1,1,1);
	vec3 Light2Color = vec3(0.0,0.0,1.0);
	float light2Power = 50.0f;
	
	// Material properties
	vec3 MaterialDiffuseColor = texture(myTextureSampler, UV).rgb;
	vec3 MaterialAmbientColor = vec3(0.1,0.1,0.1) * MaterialDiffuseColor;
	vec3 MaterialSpecularColor = vec3(0.3,0.3,0.3);

	// Distance to the light
	float distance = length(lightCoordinates - Position_worldspace);
	float distance2 = length(light2Coordinates - Position_worldspace);

	// Normal of the computed fragment, in camera space
	vec3 n = normalize(Normal_cameraspace);
	vec3 n2 = normalize(Normal2_cameraspace);

	// Direction of the light (from the fragment to the light)
	vec3 l = normalize(LightDirection_cameraspace);
	vec3 l2 = normalize(LightDirection2_cameraspace);

	// Cosine of the angle between the normal and the light direction, 
	// clamped above 0
	//  - light is at the vertical of the triangle -> 1
	//  - light is perpendicular to the triangle -> 0
	//  - light is behind the triangle -> 0
	float cosTheta = clamp(dot(n, l), 0, 1);
	float cosTheta2 = clamp(dot(n2, l), 0, 1);
	
	// Eye vector (towards the camera)
	vec3 E = normalize(EyeDirection_cameraspace);

	// Direction in which the triangle reflects the light
	vec3 R = reflect(-l,n);
	vec3 R2 = reflect(-l,n2);

	// Cosine of the angle between the Eye vector and the Reflect vector,
	// clamped to 0
	//  - Looking into the reflection -> 1
	//  - Looking elsewhere -> < 1
	float cosAlpha = clamp(dot(E, R), 0, 1);
	float cosAlpha2 = clamp(dot(E, R2), 0, 1);
	
	color = 
		// Ambient : simulates indirect lighting
		MaterialAmbientColor +
		// Diffuse : "color" of the object
		MaterialDiffuseColor * LightColor * diffuseLightPower * cosTheta / (distance*distance) +
		MaterialDiffuseColor * Light2Color * light2Power * cosTheta2 / (distance2*distance2) +
		// Specular : reflective highlight, like a mirror
		MaterialSpecularColor * LightColor * specularLightPower * pow(cosAlpha,5) / (distance*distance) +
		MaterialSpecularColor * Light2Color * light2Power * pow(cosAlpha2,5) / (distance2*distance2);

}