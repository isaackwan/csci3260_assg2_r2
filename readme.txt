	CSCI3260 Assignment 2 Keyboard / Mouse Events  

Name: Long Isaac KWAN
Student ID: 1155072554

Manipulation:
	Key "q": increase brightness of diffuse light
	Key "w": decrease brightness of diffuse light
	Key "z": increase brightness of specular light
	Key "x": decrease brightness of specular light

	Key "up arrow": move the jeep forward
	Key "down arrow": move the jeep backward
	Key "left arrow": CCW-rotate the jeep
	Key "right arrow": CW-rotate the jeep

	Key "s": stop rotation of airplane

	Mouse: change the camera coordinates
	Key " ": disable this pan function