/*********************************************************
FILE : main.cpp (csci3260 2017-2018 Assignment 2)
*********************************************************/
/*********************************************************
Student Information
Student ID: 1155072554
Student Name: Long Isaac KWAN

NOTE: this code references TA's boilerplate code & http://www.opengl-tutorial.org/beginners-tutorials/tutorial-8-basic-shading/
*********************************************************/

#include "Dependencies\glew\glew.h"
#include "Dependencies\freeglut\freeglut.h"
#include "Dependencies\glm\glm.hpp"
#include "Dependencies\glm\gtc\matrix_transform.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>


#define positionAttrib 0
#define uvAttrib 1
#define vpTransformationAttrib 2
#define textureAttrib 3
#define modelTransformationAttrib 4
#define normalCoordinatesAttrib 5
#define lightPositionAttrib 6
#define diffuseLightPowerAttrib 7
#define specularLightPowerAttrib 8
#define lightPosition2Attrib 9

using namespace std;

GLint programID;
GLuint vao[5];
GLuint vbo[9];
size_t obj1_size;
size_t obj2_size;
size_t obj3_size;
size_t obj4_size;
GLuint textureIds[4];
GLint kbState[6]; // [3]: jeep x-axis; [4]: jeep y-axis
int mousePrev[2];
bool mouseActive = true;
GLint planeRotateState = 0;
bool planeRotateActive = true;
GLfloat diffuseLightPower = 50.0f;
GLfloat specularLightPower = 50.0f;

bool checkStatus(
	GLuint objectID,
	PFNGLGETSHADERIVPROC objectPropertyGetterFunc,
	PFNGLGETSHADERINFOLOGPROC getInfoLogFunc,
	GLenum statusType)
{
	GLint status;
	objectPropertyGetterFunc(objectID, statusType, &status);
	if (status != GL_TRUE)
	{
		GLint infoLogLength;
		objectPropertyGetterFunc(objectID, GL_INFO_LOG_LENGTH, &infoLogLength);
		GLchar* buffer = new GLchar[infoLogLength];

		GLsizei bufferSize;
		getInfoLogFunc(objectID, infoLogLength, &bufferSize, buffer);
		cout << buffer << endl;

		delete[] buffer;
		return false;
	}
	return true;
}

bool checkShaderStatus(GLuint shaderID)
{
	return checkStatus(shaderID, glGetShaderiv, glGetShaderInfoLog, GL_COMPILE_STATUS);
}

bool checkProgramStatus(GLuint programID)
{
	return checkStatus(programID, glGetProgramiv, glGetProgramInfoLog, GL_LINK_STATUS);
}

string readShaderCode(const char* fileName)
{
	ifstream meInput(fileName);
	if (!meInput.good())
	{
		cout << "File failed to load..." << fileName;
		exit(1);
	}
	return string(
		istreambuf_iterator<char>(meInput),
		istreambuf_iterator<char>()
	);
}

void installShaders()
{
	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* adapter[1];
	string temp = readShaderCode("VertexShaderCode.glsl");
	adapter[0] = temp.c_str();
	glShaderSource(vertexShaderID, 1, adapter, 0);
	temp = readShaderCode("FragmentShaderCode.glsl");
	adapter[0] = temp.c_str();
	glShaderSource(fragmentShaderID, 1, adapter, 0);

	glCompileShader(vertexShaderID);
	glCompileShader(fragmentShaderID);

	if (!checkShaderStatus(vertexShaderID) || !checkShaderStatus(fragmentShaderID))
		return;

	programID = glCreateProgram();
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);
	glLinkProgram(programID);

	if (!checkProgramStatus(programID))
		return;

	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);

	glUseProgram(programID);
}

void keyboard(unsigned char key, int x, int y)
{
	/*if (key == 'c') {
		kbState[1] += 1;
		cout << ++kbState[1] << endl;
	}
	else if (key == 'v') {
		kbState[1] -= 1;
		cout << --kbState[1] << endl;
	}
	else if (key == 'b') {
		kbState[2] += 1;
		cout << ++kbState[2] << endl;
	}
	else if (key == 'n') {
		kbState[2] -= 1;
		cout << --kbState[2] << endl;
	}
	else if (key == 'a') {
		cout << ++kbState[3] << endl;
	}
	else if (key == 's') {
		cout << --kbState[3] << endl;
	}
	else if (key == 'd') {
		cout << ++kbState[4] << endl;
	}
	else if (key == 'f') {
		cout << --kbState[4] << endl;
	}
	else if (key == 'g') {
		cout << ++kbState[5] << endl;
	}
	else if (key == 'h') {
		cout << --kbState[5] << endl;
	}*/
	if (key == 32) {
		mouseActive = !mouseActive;
	}
	else if (key == 's') {
		planeRotateActive = !planeRotateActive;
	}
	else if (key == 'q') {
		diffuseLightPower += 2;
		cout << diffuseLightPower << endl;
		glUniform1f(diffuseLightPowerAttrib, diffuseLightPower);
	}
	else if (key == 'w') {
		if (diffuseLightPower <= 0.001f) {
			cout << "going to the negative territory" << endl;
			return;
		}
		diffuseLightPower -= 3;
		cout << diffuseLightPower << endl;
		glUniform1f(diffuseLightPowerAttrib, diffuseLightPower);
	}
	else if (key == 'z') {
		specularLightPower += 5;
		cout << specularLightPower << endl;
		glUniform1f(specularLightPowerAttrib, specularLightPower);
	}
	else if (key == 'x') {
		if (specularLightPower <= 0.001f) {
			cout << "going to the negative territory" << endl;
			return;
		}
		specularLightPower -= 5;
		cout << specularLightPower << endl;
		glUniform1f(specularLightPowerAttrib, specularLightPower);
	}
}

void move(int key, int x, int y) 
{
	//TODO: Use arrow keys to do interactive events and animation
	if (key == GLUT_KEY_UP) {
		kbState[3] += 1;
	}
	else if (key == GLUT_KEY_DOWN) {
		kbState[3] -= 1;
	}
	else if (key == GLUT_KEY_LEFT) {
		kbState[4] += 1;
	}
	else if (key == GLUT_KEY_RIGHT) {
		kbState[4] -= 1;
	}
}

void PassiveMouse(int x, int y)
{
	//TODO: Use Mouse to do interactive events and animation

	if (!mouseActive) {
		return;
	}

	mousePrev[0] = x;
	mousePrev[1] = y;

	static int i = 0;
	if (i++ % 2 == 0) {
		cout << "mousePrev: " << mousePrev[0] << ", " << mousePrev[1] << endl;
	}
}

GLfloat clamp2(GLfloat input, GLfloat floor) {
	input = fabsf(input);
	floor = fabsf(floor);

	if (floor >= input) {
		return floor;
	}
	else {
		return input;
	}
}

bool loadOBJ(
	const char * path,
	std::vector<glm::vec3> & out_vertices,
	std::vector<glm::vec2> & out_uvs,
	std::vector<glm::vec3> & out_normals
) {
	printf("Loading OBJ file %s...\n", path);

	std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
	std::vector<glm::vec3> temp_vertices;
	std::vector<glm::vec2> temp_uvs;
	std::vector<glm::vec3> temp_normals;


	FILE * file = fopen(path, "r");
	if (file == NULL) {
		printf("Impossible to open the file ! Are you in the right path ? See Tutorial 1 for details\n");
		getchar();
		return false;
	}

	while (1) {

		char lineHeader[128];
		// read the first word of the line
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

				   // else : parse lineHeader

		if (strcmp(lineHeader, "v") == 0) {
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader, "vt") == 0) {
			glm::vec2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uv.y = -uv.y; // Invert V coordinate since we will only use DDS texture, which are inverted. Remove if you want to use TGA or BMP loaders.
			temp_uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "vn") == 0) {
			glm::vec3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0) {
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9) {
				printf("File can't be read by our simple parser :-( Try exporting with other options\n");
				fclose(file);
				return false;
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
		}
		else {
			// Probably a comment, eat up the rest of the line
			char stupidBuffer[1000];
			fgets(stupidBuffer, 1000, file);
		}

	}

	// For each vertex of each triangle
	for (unsigned int i = 0; i<vertexIndices.size(); i++) {

		// Get the indices of its attributes
		unsigned int vertexIndex = vertexIndices[i];
		unsigned int uvIndex = uvIndices[i];
		unsigned int normalIndex = normalIndices[i];

		// Get the attributes thanks to the index
		glm::vec3 vertex = temp_vertices[vertexIndex - 1];
		glm::vec2 uv = temp_uvs[uvIndex - 1];
		glm::vec3 normal = temp_normals[normalIndex - 1];

		// Put the attributes in buffers
		out_vertices.push_back(vertex);
		out_uvs.push_back(uv);
		out_normals.push_back(normal);

	}
	fclose(file);
	return true;
}

GLuint loadBMP_custom(const char * imagepath) {

	printf("Reading image %s\n", imagepath);

	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;
	unsigned char * data;

	FILE * file = fopen(imagepath, "rb");
	if (!file) { printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", imagepath); getchar(); return 0; }

	if (fread(header, 1, 54, file) != 54) {
		printf("Not a correct BMP file\n");
		return 0;
	}
	if (header[0] != 'B' || header[1] != 'M') {
		printf("Not a correct BMP file\n");
		return 0;
	}
	if (*(int*)&(header[0x1E]) != 0) { printf("Not a correct BMP file\n");    return 0; }
	if (*(int*)&(header[0x1C]) != 24) { printf("Not a correct BMP file\n");    return 0; }

	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);
	if (imageSize == 0)    imageSize = width*height * 3; 
	if (dataPos == 0)      dataPos = 54; 

	data = new unsigned char[imageSize];
	fread(data, 1, imageSize, file);
	fclose(file);

	
	//TODO: Create one OpenGL texture and set the texture parameter 
	
	GLuint textureId;
	glGenTextures(1, &textureId);
	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureId);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	// OpenGL has now copied the data. Free our own version
	delete[] data;
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	cout << "Finished loading image " << imagepath << ", textureId: " << textureId << endl;

	return textureId;
}

void sendDataToOpenGL()
{
	//TODO:
	//Load objects and bind to VAO & VBO
	//Load texture

	vector<glm::vec3> vertices;
	vector<glm::vec2> uvs;
	vector<glm::vec3> normals;

	vector<glm::vec3> vertices2;
	vector<glm::vec2> uvs2;
	vector<glm::vec3> normals2;

	vector<glm::vec3> vertices3;
	vector<glm::vec2> uvs3;
	vector<glm::vec3> normals3;

	vector<glm::vec3> vertices4;
	vector<glm::vec3> normals4;
	vector<GLushort> elements4;

	glGenVertexArrays(5, vao);
	glGenBuffers(9, vbo);

	bool res1 = loadOBJ("airplane4.obj", vertices, uvs, normals);

	glBindVertexArray(vao[0]);

	glEnableVertexAttribArray(positionAttrib);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(
		positionAttrib, // attribute
		3, // size
		GL_FLOAT, // type
		GL_FALSE, // normalized?
		0, // stride
		(void*)0 // array buffer offset
	);

	glEnableVertexAttribArray(uvAttrib);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
	glVertexAttribPointer(
		uvAttrib,                         // attribute
		2,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	glEnableVertexAttribArray(normalCoordinatesAttrib);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[6]);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
	glVertexAttribPointer(
		normalCoordinatesAttrib,          // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	obj1_size = vertices.size();

	textureIds[0] = loadBMP_custom("airplane_texture.bmp");


	glBindVertexArray(vao[4]);
	bool res2 = loadOBJ("jeep5.obj", vertices2, uvs2, normals2);
	glEnableVertexAttribArray(positionAttrib);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[4]);
	glBufferData(GL_ARRAY_BUFFER, vertices2.size() * sizeof(glm::vec3), &vertices2[0], GL_STATIC_DRAW);
	glVertexAttribPointer(
		positionAttrib, // attribute
		3, // size
		GL_FLOAT, // type
		GL_FALSE, // normalized?
		0, // stride
		(void*)0 // array buffer offset
	);

	glEnableVertexAttribArray(uvAttrib);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[5]);
	glBufferData(GL_ARRAY_BUFFER, uvs2.size() * sizeof(glm::vec2), &uvs2[0], GL_STATIC_DRAW);
	glVertexAttribPointer(
		uvAttrib,                         // attribute
		2,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	glEnableVertexAttribArray(normalCoordinatesAttrib);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[7]);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
	glVertexAttribPointer(
		normalCoordinatesAttrib,          // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	obj2_size = vertices2.size();

	textureIds[1] = loadBMP_custom("jeep_texture.bmp");

	glBindVertexArray(vao[2]);
	bool res3 = loadOBJ("plane4.obj", vertices3, uvs3, normals3);
	glEnableVertexAttribArray(positionAttrib);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
	glBufferData(GL_ARRAY_BUFFER, vertices3.size() * sizeof(glm::vec3), &vertices3[0], GL_STATIC_DRAW);
	glVertexAttribPointer(
		positionAttrib, // attribute
		3, // size
		GL_FLOAT, // type
		GL_FALSE, // normalized?
		0, // stride
		(void*)0 // array buffer offset
	);

	glEnableVertexAttribArray(uvAttrib);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[3]);
	glBufferData(GL_ARRAY_BUFFER, uvs3.size() * sizeof(glm::vec2), &uvs3[0], GL_STATIC_DRAW);
	glVertexAttribPointer(
		1,                                // attribute
		2,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	glEnableVertexAttribArray(normalCoordinatesAttrib);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[8]);
	glBufferData(GL_ARRAY_BUFFER, normals3.size() * sizeof(glm::vec3), &normals3[0], GL_STATIC_DRAW);
	glVertexAttribPointer(
		normalCoordinatesAttrib,          // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	obj3_size = vertices3.size();

	textureIds[2] = loadBMP_custom("plane_texture.bmp");

	glUniform1f(diffuseLightPowerAttrib, diffuseLightPower);
	glUniform1f(specularLightPowerAttrib, specularLightPower);
}

void paintGL(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.6, 0.6, 0.6, 0);
	//glEnable(GL_DEPTH_TEST);
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnableVertexAttribArray(positionAttrib);
	//TODO:
	//Set lighting information, such as position and color of lighting source
	//Set transformation matrix
	//Bind different textures
	
	glm::mat4 view = glm::lookAt(
		glm::vec3(-3.4f, -(mousePrev[0]-400)/200.0f, clamp2((600 - mousePrev[1]) / 80.0f + 7.6f, 0.5)),
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, 0.0f, 1.0f)
	);
	glm::mat4 proj = glm::perspective(glm::radians(45.0f), 800.0f / 600.0f, 0.1f, 100.0f);
	glm::mat4 vpTransformation = proj * view;
	glm::mat4 modelTransformation;
	glUniformMatrix4fv(vpTransformationAttrib, 1, GL_FALSE, &vpTransformation[0][0]);

	glm::vec3 lightPos = glm::vec3(4, 4, 4);
	glUniform3f(lightPositionAttrib, lightPos.x, lightPos.y, lightPos.z);

	glm::vec3 lightPos2 = glm::vec3(0, 0, 4);
	glUniform3f(lightPosition2Attrib, lightPos2.x, lightPos2.y, lightPos2.z);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, textureIds[2]);
	glUniform1i(textureAttrib, 2);

	glBindVertexArray(vao[2]);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
	modelTransformation = glm::mat4();
	glUniformMatrix4fv(modelTransformationAttrib, 1, GL_FALSE, &modelTransformation[0][0]);
	glDrawArrays(GL_TRIANGLES, 0, obj3_size);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureIds[0]);
	glUniform1i(textureAttrib, 0);

	glBindVertexArray(vao[0]);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	modelTransformation = glm::mat4();
	modelTransformation = glm::scale(modelTransformation, glm::vec3(0.4f, 0.4f, 0.4f));
	modelTransformation = glm::translate(modelTransformation, glm::vec3(-4.1f, -3.0f, 51/70.0f));
	modelTransformation = glm::rotate(modelTransformation, (planeRotateActive ? planeRotateState++ : planeRotateState) / 300.0f, glm::vec3(0.0f, 0.0f, 1.0f));
	glUniformMatrix4fv(modelTransformationAttrib, 1, GL_FALSE, &modelTransformation[0][0]);
	glDrawArrays(GL_TRIANGLES, 0, obj1_size);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, textureIds[1]);
	glUniform1i(textureAttrib, 1);

	glBindVertexArray(vao[4]);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[4]);
	modelTransformation = glm::mat4();
	modelTransformation = glm::scale(modelTransformation, glm::vec3(0.4f, 0.4f, 0.4f));
	modelTransformation = glm::translate(modelTransformation, glm::vec3(1.0f, 2.0f, 46 / 70.0f));
	modelTransformation = glm::translate(modelTransformation, glm::vec3(kbState[3] / 30.0f, 0, 0));
	modelTransformation = glm::rotate(modelTransformation, kbState[4] / 20.0f, glm::vec3(0.0f, 0.0f, 1.0f));
	glUniformMatrix4fv(modelTransformationAttrib, 1, GL_FALSE, &modelTransformation[0][0]);
	glDrawArrays(GL_TRIANGLES, 0, obj2_size);
	
	glutSwapBuffers();
	glutPostRedisplay();
}

void initializedGL(void) 
{
	glewInit();
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);
	glEnable(GL_TEXTURE_2D);
	installShaders();
	sendDataToOpenGL();
}

int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	//TODO:
	//Create a window with title specified
	
	glutInitWindowSize(800, 600);
	glutCreateWindow("Assignment 1");
	glewInit();

	cout << "OpenGL Version: " << glGetString(GL_VERSION) << std::endl;

	initializedGL();
	glutDisplayFunc(paintGL);
	
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(move);
	glutPassiveMotionFunc(PassiveMouse);
	
	glutMainLoop();

	return 0;
}